#include <iostream>
#include <string>
#include <cstring>
#include <cassert>
#include <iomanip>
#include <limits>

double str2double(const std::string& str)
{
	if (str.size() < 2 ) {
	    return 0.0;
	}
	return str[0] + ((str[1] & 0xff) >> 7) * 0.5;
}

int digitCheck(const char* str, const unsigned len) {
    unsigned i = 0;
    while (str[i]) {
        if (str[i] < '0' || str[i] > '9') return 0;
        ++i;
        if (len == i) return 1;
    }
    return 0;
}

int luhnCheck(const char* iccid, const unsigned len) {
	unsigned i;
	int check = 0;
	int num;
	if (!digitCheck(iccid, len)) return 0;
	if ((len < 19) || (len > 20)) return 0;
	if ((iccid[0] != '8' && iccid[1] != '9')) return 0;
	for (i = 0; i < len; ++i) {
		num = iccid[i] - '0';
		if (!((i & 1) ^ (len & 1))) {
			num *= 2;
			if (num > 9) num -= 9;
		}
		check += num;
	}
	return !(check % 10);
}

int main(int, char**)
{

	std::cout << "char is " << (std::numeric_limits<char>::is_signed ? "signed" : "not signed") << std::endl;

	double f = str2double("\xff\xff");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == -0.5);
	f = str2double("\xff\x7f");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == -1);
	f = str2double("\xe7\x7f");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == -25);
	f = str2double("\xe7\xff");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == -24.5);
	f = str2double("\xc9\x7f");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == -55);
	f = str2double("\x19\x7f");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == 25);
	f = str2double("\x19\xff");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == 25.5);
	f = str2double("\x19");
	std::cout << std::fixed << std::setw(5) << std::setprecision(1) << (double)(f) << std::endl;
	assert(f == 0.0);


	char iccid[20];

	memset(iccid, '\0', 20);
	memcpy(iccid, "8936304418060244500", 19);
	assert(!luhnCheck(iccid, 20));
	assert(luhnCheck(iccid, 19));

	memset(iccid, '\0', 20);
	memcpy(iccid, "893630441806024ABCD", 19);
	assert(!luhnCheck(iccid, 19));

	memset(iccid, '\0', 20);
	memcpy(iccid, "1136304418060244500", 19);
	assert(!luhnCheck(iccid, 19));

	memset(iccid, '\0', 20);
	memcpy(iccid, "8931088217012916964", 19);
	assert(luhnCheck(iccid, 19));

	memset(iccid, '\0', 20);
	assert(!luhnCheck(iccid, 20));
	memcpy(iccid, "89367031551636257364", 20);
	assert(!luhnCheck(iccid, 18));
	assert(!luhnCheck(iccid, 19));
	assert(luhnCheck(iccid, 20));

	return 0;
}

